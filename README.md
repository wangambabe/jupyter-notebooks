# Jupyter Notebooks

Example code for the blog posts (on https://www.marsja.se) and YouTube Videos (http://bit.ly/SUB2EM). Mainly, I will upload Jupyter Notebooks (and datasets) for the different tutorials I make.

## Example Codes
Here are the links to different Jupyter Notebooks containing both R and Python example code for carrying out data analysis, data wrangling, and other data sciencey things.

- [Two-Way ANOVA using Python and Statsmodels](https://gitlab.com/marsja/jupyter-notebooks/blob/master/Example_Code_for_Two-Way_ANOVA_in_Python_and_Statsmodels.ipynb)

- [Tutorial on How to Reverse Scores using R](https://gitlab.com/marsja/jupyter-notebooks/blob/master/How_to_Reverse_Scores_in_R.ipynb)

- [How to Reverse Scores in Python using Pandas](https://gitlab.com/marsja/jupyter-notebooks/blob/master/How_to_Reverse_Scores_Using_Python.ipynb)

- [Python Data Visualization Tutorial (9 Techniques You Should Learn)](https://gitlab.com/marsja/data_visualization_in_python_tutorial.ipynb)
